# Questionário para cadastro com comandos de repetição WHILE, condicionais, auxiliares de repetição e Repetição FOR.

perguntas = [
  "Qual seu nome? ",
  "Qual sua idade? ",
  "Qual sua altura? ",
  "Qual a cor do seu cabelo? ",
  "Qual sua cor preferida? ",
  "Quantas pessoas estão na aula hoje? "
]

print("-- Questionário para cadastro --")
respostas = []
i = 0
while len(perguntas) > i:
  respostas.append(input(perguntas[i]))

  if respostas[-1] == "":
    print("Responda a pergunta.")
    continue
    # Ignora o restante do código
  elif respostas[-1] == "sair":
    print("O questionario será encerrado.")
    break
    # Encerra a estrutura de repetição
  else:
    i += 1
    # vai para a proxima pergunta
print("Obrigado por responder o questionário!")

for resposta_id in range(len(respostas)):
  print("Resposta", resposta_id+1 ,": ", respostas[resposta_id])