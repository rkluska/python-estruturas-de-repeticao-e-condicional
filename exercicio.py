# Problem 1
# ___________________________________________________________________________________________________________________________________
# If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.

print("If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.")
print("Find the sum of all the multiples of 3 or 5 below 1000.")
# natural number is equal to int numbers
#multiples of #n0 is igual to #n0%#n1=0
# ex:
# print("10 is not a multiplo of 3 because 10 % 3 = ", 10 % 3)
# print("9 is a multiplo of 3 because 9 % 3 = ", 9 % 3)

# 1st - Solution
# Check number by number is a multiple of 3 or 5
def check_m(n1, n2):
    if((n1 % n2) == 0):
        return True;
    else:
        return False;

n = int(input("Type a number with you would like fo find he sum of all the multiples of 3 or 5 below: "))
# ------------------------------
#  TRACK TIME - START
# ------------------------------
# using time module
import time
# ts stores the time in seconds
ts0 = time.time()
# ----------------------------
# n = 10
i = 2
sum_m3 = 0
sum_m5 = 0
while(i < n):
    print("number checked: ", i)
    if(check_m(i, 3)):
        sum_m3 = sum_m3 + i
        # print("--->",i,"is multiple a of 3!")
    else: 
        if(check_m(i, 5)):
            sum_m5 = sum_m5 + i
            # print("--->",i,"multiple a of 5!")
    i += 1

print("sum_m3: ", sum_m3)
print("sum_m5: ", sum_m5)
print("The sum of all the multiples of 3 or 5 below", n,": ", (sum_m3 + sum_m5))
# ------------------------------
#  TRACK TIME - END
# ------------------------------
print("------------------------------------------------------------------------------------------")
print("Score for execution: ", (i - 1))
ts1 = time.time()
print("Execution time: ",(ts1 - ts0),"seconds")
print("Execution time: ",(ts1 - ts0)/60,"minuties")
