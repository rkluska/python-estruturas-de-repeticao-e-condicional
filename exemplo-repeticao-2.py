# Questionário para cadastro com comandos de repetição FOR

perguntas = [
  "Qual seu nome? ",
  "Qual sua idade? ",
  "Qual sua altura? ",
  "Qual a cor do seu cabelo? ",
  "Qual sua cor preferida? ",
  "Quantas pessoas estão na aula hoje? "
]

print("-- Questionário para cadastro --")

respostas = []

for pergunta in perguntas:
  respostas.append(input(pergunta))
  
print("Obrigado por responder o questionário!")
print("Respostas: ", respostas)